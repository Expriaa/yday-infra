#!/usr/bin/env python3

import ssl
import json
from pyVim.connect import SmartConnectNoSSL, Disconnect
from pyVmomi import vim

vcenter_host = ''
vcenter_user = ''
vcenter_password = ''

def get_vms(content):
    """
    Récupère toutes les VMs sur le vCenter
    """
    vm_view = content.viewManager.CreateContainerView(content.rootFolder, [vim.VirtualMachine], True)
    vms = vm_view.view
    vm_view.Destroy()
    return vms

def main():
    service_instance = SmartConnectNoSSL(host=vcenter_host, user=vcenter_user, pwd=vcenter_password)
    if not service_instance:
        print("Impossible de se connecter au vCenter avec les détails fournis.")
        return

    content = service_instance.RetrieveContent()
    vms = get_vms(content)

    valid_vms = [vm for vm in vms if vm.summary.guest.ipAddress]

    num_vms_per_group = len(valid_vms) // 2

    inventory = {'all': {'children': {'webserver': {'hosts': []}, 'reverse_proxy': {'hosts': []}}, 'vars': {'ansible_user': 'votre_utilisateur_ansible'}}}

    for i, vm in enumerate(valid_vms):
        vm_ip = vm.summary.guest.ipAddress
        if i < num_vms_per_group:
            inventory['all']['children']['webserver']['hosts'].append(vm_ip)
        else:
            inventory['all']['children']['reverse_proxy']['hosts'].append(vm_ip)
    print(json.dumps(inventory, indent=4))

    Disconnect(service_instance)

if __name__ == "__main__":
    main()
