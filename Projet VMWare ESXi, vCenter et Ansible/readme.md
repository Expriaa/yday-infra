# Documentation Technique : VMWare ESXi, vCenter et Ansible

## Table des Matières

- [1. Vue d'Ensemble](#1-vue-densemble)
- [2. Préparation de l'Environnement](#2-préparation-de-lenvironnement)
- [3. Configuration de VMware vCenter](#3-configuration-de-vmware-vcenter)
- [4. Déploiement et Provisionnement](#4-déploiement-et-provisionnement)
- [5. Validation et Tests](#5-validation-et-tests)
- [6. Gestion du Projet](#6-gestion-du-projet)
- [7. Conclusion](#7-conclusion)
- [Annexe](#annexe)

## 1. Vue d'Ensemble
<a name="1-vue-densemble"></a>

Ce document technique présente les étapes et les configurations nécessaires pour intégrer VMWare ESXi, VMWare vCenter et Ansible. Il vise à fournir des directives claires pour la mise en place d'une infrastructure virtualisée et son automatisation avec Ansible.

## 2. Préparation de l'Environnement
<a name="2-préparation-de-lenvironnement"></a>

### Prérequis Logiciels

- Téléchargez et installez VMware Workstation sur votre machine locale à partir du [site officiel de VMware](https://www.vmware.com/products/workstation-pro.html).
- Assurez-vous que la virtualisation matérielle est activée dans le BIOS/UEFI de votre machine.

### Étapes d'Installation d'ESXi

1. **Téléchargement de l'ISO d'ESXi-7.0u3n**
   - Accédez au [VMware Download Center](https://my.vmware.com/en/web/vmware/downloads) et téléchargez la version ESXi-7.0u3n en choisissant l'option "ISO image".

2. **Création d'une Nouvelle Machine Virtuelle**
   - Lancez VMware Workstation et sélectionnez "Nouvelle Machine Virtuelle".
   - Choisissez "Installer le système d'exploitation ultérieurement" et sélectionnez "Autre" comme type de système d'exploitation.
   - Sélectionnez "ESXi" comme version.

3. **Configuration de la Machine Virtuelle**
   - Configurez le matériel, y compris le nombre de processeurs virtuels, la quantité de mémoire RAM, et le disque dur virtuel. Utilisez le type de disque SCSI.

4. **Montage de l'ISO ESXi-7.0u3n**
   - Dans les paramètres de la machine virtuelle, ajoutez un périphérique CD/DVD et sélectionnez "Utiliser un fichier ISO". Chargez l'ISO d'ESXi-7.0u3n téléchargé.

5. **Installation d'ESXi**
   - Démarrez la machine virtuelle et booter à partir de l'ISO. Suivez les instructions pour installer ESXi.

6. **Post-installation**
   - Après l'installation, retirez l'ISO et redémarrez la machine virtuelle.

7. **Configuration du Réseau**
   - Configurez les paramètres réseau d'ESXi, comme l'adresse IP statique et les informations de DNS.

8. **Accès à ESXi**
   - Ouvrez un navigateur et entrez l'adresse IP d'ESXi pour accéder à l'interface. Si nécessaire, utilisez le client vSphere pour la gestion.

9. **Création de Machines Virtuelles**
   - Utilisez vSphere pour créer et gérer des VMs sur votre instance ESXi.


## 3. Configuration de VMware vCenter
<a name="3-configuration-de-vmware-vcenter"></a>

### Création de VMs

- Utiliser vCenter pour configurer une VM patron basée sur Rocky Linux.
- Préparer les modèles de VM pour différents rôles (serveur web, base de données, etc.).

## 4. Déploiement et Provisionnement
<a name="4-déploiement-et-provisionnement"></a>

Cette partie aborde le processus de déploiement et de provisionnement des machines virtuelles (VMs) dans un environnement VMware à l'aide d'Ansible. Nous nous concentrons ici sur la structure des playbooks Ansible, le fonctionnement de l'inventaire dynamique, et les détails spécifiques des processus de déploiement.

### A. Construction des Playbooks Ansible

#### Structure Générale des Playbooks :

Les playbooks Ansible sont écrits en YAML, chaque playbook représentant un ensemble de tâches spécifiques à exécuter sur les groupes d'hôtes ciblés.

#### Exemple de Playbook pour le Serveur Web :

[Playbook](https://gitlab.com/Expriaa/yday-infra/-/tree/main/Projet%20VMWare%20ESXi,%20vCenter%20et%20Ansible/Playbook%20Ansible?ref_type=heads)

### B. Inventaire Dynamique avec Ansible

#### Configuration de l'Inventaire Dynamique :

L'inventaire dynamique est configuré pour interroger VMware vCenter et récupérer la liste et l'état actuel des VMs.

#### Fonctionnement de l'Inventaire :

À chaque exécution d'Ansible, le script ou le plugin d'inventaire dynamique (comme vmware_vm_inventory) interroge vCenter pour obtenir une liste à jour des VMs et les classe dans les groupes appropriés.

### C. Processus de Déploiement

#### Déploiement des Services Web :

Installation et configuration de serveurs web (Apache, Nginx) sur les VMs désignées. Cela inclut la configuration de base du serveur, la création de dossiers pour les sites web, et la configuration de base des fichiers de sites virtuels.

#### Configuration Réseau et Sécurité :

Configuration des règles de pare-feu pour autoriser le trafic web et autres services nécessaires. Mise en place de configurations de sécurité de base, telles que les mises à jour de sécurité et les paramètres de sécurité du système d'exploitation.

### D. Automatisation et Orchestration

#### Orchestration des Déploiements :

Les playbooks sont intégrés dans une stratégie d'orchestration globale pour permettre des déploiements cohérents et reproductibles.

#### Modularité et Réutilisation :

Utilisation de rôles Ansible pour structurer et réutiliser les configurations de manière efficace.

Cette méthode de déploiement et de provisionnement offre une approche automatisée et scalable pour la gestion des VMs dans un environnement VMware, minimisant les interventions manuelles et augmentant la fiabilité des configurations appliquées.

## 5. Validation et Tests
<a name="5-validation-et-tests"></a>

### Exécution des Playbooks

- Lancer les playbooks Ansible pour déployer et configurer des serveurs web.
- Vérifier la bonne exécution des tâches.

### Tests de Fonctionnement

- Tester la connectivité réseau et les services sur les VMs déployées.
- S'assurer que les configurations sont appliquées comme prévu.

## 6. Gestion du Projet
<a name="6-gestion-du-projet"></a>

### Suivi et Documentation

- Documenter toutes les configurations et les changements.
- Utiliser des outils de suivi de projet pour gérer les tâches et les échéances.

## 7. Conclusion
<a name="7-conclusion"></a>

Ce projet démontre l'efficacité de l'intégration entre VMWare et Ansible pour la gestion d'une infrastructure virtualisée. La mise en place d'un tel système offre flexibilité, automatisation et contrôle précis sur l'environnement virtuel.