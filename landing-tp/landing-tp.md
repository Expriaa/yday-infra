# TP 1 

## Niveau 1

🎯 Quels sont les deux différents types d'hyperviseur existant, et quelles sont leur différence ?

Les deux types d'hyperviseurs : L'hyperviseur de type 1, nommé « bare metal » s'exécute directement sur le matériel de l'hôte. L'hyperviseur de type 2, nommé « hébergé » s'exécute sous forme d'une couche logicielle sur un système d'exploitation, comme n'importe quel autre programme informatique.

🎰 Changez le nom d'hôte des machines pour avoir respectivement vm-landing1 et vm-landing2

```
[arthur@LandingVM1 ~]$ sudo hostnamectl set-hostname LandingVM1
[arthur@LandingVM1 ~]$ sudo cat /etc/hosts
127.0.0.1   LandingVM1
```

```
[arthur@LandingVM1 ~]$ sudo systemctl restart systemd-hostnamed
[arthur@LandingVM1 ~]$ hostnamectl
 Static hostname: LandingVM1
```

```
[arthur@LandingVM2 ~]$ sudo hostnamectl set-hostname LandingVM2
[arthur@LandingVM2 ~]$ sudo cat /etc/hosts
127.0.0.1   LandingVM2
```

```
[arthur@LandingVM2 ~]$ sudo systemctl restart systemd-hostnamed
[arthur@LandingVM2 ~]$ hostnamectl
 Static hostname: LandingVM2
```

🎰 Trouvez l'adresse IP locale des machines

LandingVM1
```
[arthur@LandingVM1 ~]$ ip a
inet 172.16.64.2/24
```

LandingVM2

```
[arthur@LandingVM2 ~]$ ip a
inet 172.16.64.3/24
```

🎯 Quelle est l'adresse de broadcast ?

172.16.64.255 pour les deux machines.

🎰 Trouvez le masque de sous-réseau des machines

```
[arthur@LandingVM1 ~]$ ip a
inet 172.16.64.2/24
```

```
[arthur@LandingVM2 ~]$ ip a
inet 172.16.64.3/24
```

Ou bien on peut regarder la conf de l'interface des cartes réseaux :

```
[arthur@LandingVM2 ~]$ cat /etc/sysconfig/network-scripts/ifcfg-enp0s8 | grep NETMASK
NETMASK=255.255.255.0
```

🎰 Pingez l'adresse publique du site www.ynov.com avec une des deux machines

```
[arthur@LandingVM1 ~]$ ping www.ynov.com
PING www.ynov.com (172.67.74.226) 56(84) bytes of data.
64 bytes from 172.67.74.226 (172.67.74.226): icmp_seq=1 ttl=56 time=17.2 ms
64 bytes from 172.67.74.226 (172.67.74.226): icmp_seq=2 ttl=56 time=14.5 ms
```

```
[arthur@LandingVM2 ~]$ ping www.ynov.com
PING www.ynov.com (172.67.74.226) 56(84) bytes of data.
64 bytes from 172.67.74.226 (172.67.74.226): icmp_seq=1 ttl=56 time=17.2 ms
64 bytes from 172.67.74.226 (172.67.74.226): icmp_seq=2 ttl=56 time=14.5 ms
```

🎰 Essayez de lire le contenu du fichier /var/log/syslog.

```
[labo-user2@LandingVM2 arthur]$ cat /var/log/tallylog
cat: /var/log/tallylog: Permission denied
```

🎯 Que se passe-t-il ? Pourquoi ?


L'erreur "Permission denied" indique que l'utilisateur "labo-user2" n'a pas les autorisations nécessaires pour lire le fichier /var/log/tallylog. Cette erreur est due à des restrictions de sécurité qui empêchent l'accès à ce fichier sans les autorisations appropriées.

🎯 Il y aura toujours une erreur. Après avoir recherche sur Google la significance du groupe wheel dans Linux, pourquoi cette erreur est-elle toujours présente ?

Même si un utilisateur est membre du groupe "wheel" et peut utiliser sudo, cela ne lui accorde pas nécessairement l'accès en lecture au fichier /var/log/tallylog. Les autorisations de ce fichier sont gérées séparément, et le groupe "wheel" n'a pas d'impact sur ces autorisations.

🎯 Il n'y aura plus d'erreur. Pourquoi ?

Lorsqu'on exécute ````sudo cat /var/log/tallylog```, on utilise la commande sudo pour élever temporairement nos privilèges et exécuter la commande cat avec les droits de superutilisateur. Cela permet d'accéder au fichier /var/log/tallylog même si votre utilisateur est membre du groupe "wheel". Cependant, cela ne signifie pas que l'accès au fichier est accordé en permanence à tous les membres du groupe "wheel" sans utiliser sudo.

🎯 Ca devrait marcher. Pourquoi ?

Il n'y a pas d'erreur non plus car l'utilisateur root à les permission administrateur.

🎯 Ca devrait marcher. Pourquoi ?

Je n'ai personellement pas d'erreur lorsque mon user est ajouté au groupe wheel. Je n'ai donc pas besoin de modifié le fichier visudo.

🎰 Pingez vm-landing2 avec vm-landing1

```
[arthur@LandingVM1 etc]$ ping 172.16.64.3
PING 172.16.64.3 (172.16.64.3) 56(84) bytes of data.
64 bytes from 172.16.64.3: icmp_seq=1 ttl=64 time=0.663 ms
64 bytes from 172.16.64.3: icmp_seq=2 ttl=64 time=0.955 ms
64 bytes from 172.16.64.3: icmp_seq=3 ttl=64 time=0.874 ms
```

🎰 Vérifiez leur version

```
[arthur@LandingVM1 etc]$ sl --version
```

Et ? Un train ? ok pourquoi pas.

```
[arthur@LandingVM1 etc]$ htop --version
htop 3.2.2
[arthur@LandingVM1 etc]$ dnsmasq --version
Dnsmasq version 2.85
```

🎰 pingez google.com

```
[arthur@LandingVM1 etc]$ ping google.com
PING google.com (142.250.179.110) 56(84) bytes of data.
64 bytes from par21s20-in-f14.1e100.net (142.250.179.110): icmp_seq=1 ttl=115 time=31.7 ms
64 bytes from par21s20-in-f14.1e100.net (142.250.179.110): icmp_seq=2 ttl=115 time=31.3 ms
```

🎰 Vérifiez leur version

```
[labo-user2@LandingVM2 arthur]$ sudo fail2ban-client -V
1.0.2
[labo-user2@LandingVM2 arthur]$ unzip --version
caution:  both -n and -o specified; ignoring -o
UnZip 6.00
[labo-user2@LandingVM2 arthur]$ htop --version
htop 3.2.2
```

🎰 pingez cloudflare.com

```
[labo-user2@LandingVM2 arthur]$ ping cloudflare.com
PING cloudflare.com (104.16.132.229) 56(84) bytes of data.
64 bytes from 104.16.132.229 (104.16.132.229): icmp_seq=1 ttl=56 time=17.2 ms
64 bytes from 104.16.132.229 (104.16.132.229): icmp_seq=2 ttl=56 time=19.9 ms
```

🎯 Quelle est l'utilité de ce type de carte réseau ?

le mode "Host-Only" permet de créer un réseau privé pour les machines virtuelles, offrant une isolation du réseau externe tout en permettant la communication entre les machines virtuelles et la machine hôte. C'est un choix courant pour les environnements de développement et de test.

🎰 Pingez landing-vm2 avec landing-vm1, que se passe-t-il ?

```
[arthur@LandingVM1 ~]$ ping 172.16.64.3
PING 172.16.64.3 (172.16.64.3) 56(84) bytes of data.
64 bytes from 172.16.64.3: icmp_seq=1 ttl=64 time=0.815 ms
64 bytes from 172.16.64.3: icmp_seq=2 ttl=64 time=0.774 ms
64 bytes from 172.16.64.3: icmp_seq=3 ttl=64 time=0.819 ms
```